override CFLAGS := $(CFLAGS) -Wall -Wextra -std=c23 -D_GNU_SOURCE $(shell pkg-config --cflags libsystemd)

xscreenfold : main.o fah-control.o xscreensaver-watch.o
	$(CC) $(CFLAGS) -o $@ $+ $(shell pkg-config --libs libsystemd)

main.o : fah-control.h xscreensaver-watch.h
