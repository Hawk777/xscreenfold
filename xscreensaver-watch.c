#include "xscreensaver-watch.h"
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <systemd/sd-bus.h>
#include <systemd/sd-event.h>

struct xscreensaver_watch {
	/**
	 * @brief The D-Bus message matching slot.
	 */
	sd_bus_slot *slot;
};

/**
 * @brief Handles matching signals from D-Bus.
 *
 * @param[in] m The message.
 * @param[in] userdata The watcher.
 * @param[in] error The error that occurred.
 * @return 0 if everything is OK and polling should continue, or the negative
 * of an error value if an error occurred.
 */
static int xscreensaver_watch_signal(sd_bus_message *m, void *userdata, sd_bus_error *) {
	int err;

	assert(sd_bus_message_is_signal(m, "org.freedesktop.DBus.Properties", "PropertiesChanged"));

	const char *interface;
	if((err = sd_bus_message_read_basic(m, 's', &interface)) < 0) {
		sd_event_exit(SD_EVENT_DEFAULT, err);
		return err;
	}
	if(strcmp(interface, "org.freedesktop.login1.Manager") != 0) {
		return 0;
	}

	if((err = sd_bus_message_enter_container(m, 'a', "{sv}")) < 0) {
		sd_event_exit(SD_EVENT_DEFAULT, err);
		return err;
	}

	while((err = sd_bus_message_at_end(m, 0)) == 0) {
		if((err = sd_bus_message_enter_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv")) < 0) {
			sd_event_exit(SD_EVENT_DEFAULT, err);
			return err;
		}

		const char *key;
		if((err = sd_bus_message_read_basic(m, 's', &key)) < 0) {
			sd_event_exit(SD_EVENT_DEFAULT, err);
			return err;
		}
		if(strcmp(key, "IdleHint") == 0) {
			int value;
			if((err = sd_bus_message_read(m, "v", "b", &value)) < 0) {
				sd_event_exit(SD_EVENT_DEFAULT, err);
				return err;
			}
			const xscreensaver_watch_watcher_t *watcher = userdata;
			watcher->cb(watcher->cookie, value);
		} else {
			if((err = sd_bus_message_skip(m, nullptr)) < 0) {
				sd_event_exit(SD_EVENT_DEFAULT, err);
				return err;
			}
		}

		if((err = sd_bus_message_exit_container(m)) < 0) {
			sd_event_exit(SD_EVENT_DEFAULT, err);
			return err;
		}
	}
	if(err < 0) {
		sd_event_exit(SD_EVENT_DEFAULT, err);
		return err;
	}

	return 0;
}

/**
 * @brief Starts monitoring for screen saver activity.
 *
 * @param[in] watcher The client to notify.
 * @return The watch object, or a null pointer on error.
 */
xscreensaver_watch_t xscreensaver_watch_new(const xscreensaver_watch_watcher_t *watcher) {
	xscreensaver_watch_t ret = malloc(sizeof(*ret));
	if(ret) {
		bool ok = false;
		sd_event *event;
		int err = sd_event_default(&event);
		if(err >= 0) {
			sd_bus *bus;
			sd_bus_default_system(&bus);
			if(err >= 0) {
				err = sd_bus_attach_event(bus, event, SD_EVENT_PRIORITY_NORMAL);
				if(err >= 0) {
					err = sd_bus_match_signal_async(bus, &ret->slot, nullptr, "/org/freedesktop/login1", "org.freedesktop.DBus.Properties", "PropertiesChanged", &xscreensaver_watch_signal, nullptr, (void *) watcher);
					if(err >= 0) {
						ok = true;
					} else {
						errno = -err;
					}
				} else {
					errno = -err;
				}
				int tmp = errno;
				sd_bus_unref(bus);
				err = tmp;
			} else {
				errno = -err;
			}
			sd_event_unref(event);
		} else {
			errno = -err;
		}
		if(!ok) {
			int tmp = errno;
			free(ret);
			ret = nullptr;
			errno = tmp;
		}
	}
	return ret;
}

/**
 * @brief Stops monitoring for screen saver activity.
 *
 * @param[in] watcher The object to destroy, or a null pointer to do nothing.
 */
void xscreensaver_watch_delete(xscreensaver_watch_t watch) {
	if(watch) {
		sd_bus_slot_unref(watch->slot);
		free(watch);
	}
}
